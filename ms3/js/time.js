function mostrarHorario() {
    var data = new Date();
    var hora = data.getHours();
    var minuto = data.getMinutes();
    var segundo = data.getSeconds();
    var horaFormatada = hora + ':' + minuto + ':' + segundo;
    document.getElementById('horario').innerHTML = horaFormatada;
}

setInterval(mostrarHorario, 1000);